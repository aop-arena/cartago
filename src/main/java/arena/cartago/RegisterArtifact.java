package arena.cartago;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import cartago.Artifact;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

public class RegisterArtifact extends Artifact {
	public static final int AVAILABLE = 0;
	public static final int PLAYING = 1;
	
	private Map<String, List<String>> types = new TreeMap<String,List<String>>();
	private Map<String, Integer> status = new TreeMap<String, Integer>(); 

	@OPERATION
	public void registerPlayer(String player, String type) {
		List<String> list = types.get(type);
		if (list == null) {
			list = new ArrayList<String>();
			types.put(type, list);
		}
		list.add(player);
		status.put(player, AVAILABLE);
	}
	
	@OPERATION
	public void shuffle(String type) {
		List<String> list = types.get(type);
		if (list == null) {
			throw new RuntimeException("This type has no players: " + type);
		}
		
		Collections.shuffle(list);
	}
	
	@OPERATION
	public void selectPlayer(String type, OpFeedbackParam<String> player) {
		List<String> list = types.get(type);
		if (list == null) {
			throw new RuntimeException("This type has no players: " + type);
		}
		
		for (String name : list) {
			if (status.get(name) == AVAILABLE) {
				status.put(name, PLAYING);
				player.set(name);
				return;
			}
		}
		
		throw new RuntimeException("No players available of type: " + type);
	}
	
	@OPERATION
	public void releasePlayer(String player) {
		status.put(player, AVAILABLE);
	}
}
