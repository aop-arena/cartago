package arena.cartago;

import java.lang.reflect.InvocationTargetException;

import com.google.gson.Gson;

import arena.game.Game;
import arena.game.GameState;
import arena.game.RoundState;
import cartago.AgentId;
import cartago.Artifact;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

public class GameArtifact extends Artifact {
	private Game game;
	private int players;
	private AgentId owner;
	private GameState state;

	public void init(int players) {
		defineObsProperty("round", 0);
		this.players = players;
		state = new GameState();
	}

	@OPERATION
	public void startRound() {
		game.clearMoves();
		updateObsProperty("round", ((Integer) getObsProperty("round").getValue())+1);
		
		execInternalOp("awaitMoves");
	}
	
	@OPERATION
	public void makeMove(int move) {
		game.makeMove(this.getCurrentOpAgentId().getAgentName(),move);
	}
	
	@OPERATION
	public void registerPlayer() {
		game.registerPlayer(this.getCurrentOpAgentId().getAgentName());
	}
	
	@OPERATION
	public void shutdown() {
		this.dispose();
	}
	
	@OPERATION
	public void installGame(String clazz) {
		if (game != null) throw new RuntimeException("The game has already been created");
		
		try {
			game = (Game) Class.forName(clazz).getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			throw new RuntimeException("Failure in GameArtifact", e);
		}
		
		owner = getCurrentOpAgentId();
		execInternalOp("awaitRegistrations");
	}

	@OPERATION
	public void generateOutcome() {
		signal("result", game.generateOutcome());
		state.rounds.add(game.getState());
	}
	
	@OPERATION
	public void collectStatistics(String format, OpFeedbackParam<String> stats) {
		if (format.equals("json")) {
			Gson gson = new Gson();
			stats.set(gson.toJson(state));
		} else if (format.equals("csv")) {
			StringBuffer buf = new StringBuffer();
			buf.append("Round");
			for (String player : state.players) {
				buf.append(",").append(player);
			}
			buf.append(",Outcome\n");
			int i=0;
			for (RoundState round : state.rounds) {
				buf.append(i++);
				for (String player : state.players) {
					buf.append(",").append(round.moves.get(player));
				}
				buf.append(",").append(round.outcome).append("\n");
			}
			stats.set(buf.toString());
		}
	}
	
	@INTERNAL_OPERATION
	public void awaitRegistrations() {
		if (game.numPlayers() < players) {
			await_time(100);
			execInternalOp("awaitRegistrations");
		} else {
			state.players = game.getPlayers();
			signal(owner, "ready");
		}
	}
	
	@INTERNAL_OPERATION
	public void awaitMoves() {
		if (game.numMoves() < players) {
			await_time(100);
			execInternalOp("awaitMoves");
		} else {
			signal(owner, "moves_completed");
		}
	}
}
