package arena.cartago;

import java.lang.reflect.InvocationTargetException;

import arena.core.Strategy;
import cartago.Artifact;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

public class StrategyArtifact extends Artifact {
	private int maxNumberOfOptions;
	private Strategy strategy;
	
	public void init(int maxNumberOfOptions) {
		this.maxNumberOfOptions = maxNumberOfOptions;
	}
	
	@OPERATION
	public void installStrategy(String clazz) {
		try {
			strategy = (Strategy) Class.forName(clazz).getConstructor(int.class).newInstance(maxNumberOfOptions);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			throw new RuntimeException("Failure in StrategyArtifact", e);
		}
	}
	
	@OPERATION
	public void generateMove(OpFeedbackParam<Integer> move) {
		move.set(strategy.generateMove());
	}
	
	@OPERATION
	public void setNumberOfMoves(int numberOfMoves) {
		strategy.setNumberOfMoves(numberOfMoves);
	}

	@OPERATION
	public void recordOutcome(int outcome) {
		strategy.setOutcome(outcome);
	}

}
