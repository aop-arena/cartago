package arena;

agent Player {
	module Cartago cartago;
	module System system;

	types player {
		formula strategy(string, cartago.ArtifactId);
		formula game(string, cartago.ArtifactId);
		formula move(int);
		formula gameOver();
		formula join(string);
	}

	rule +!main([string strategy]) {
		cartago.link();
		
		!setupStrategy(strategy);
	}
		
	rule @message(request, string sender, join(string game)) {
		!joinGame(game);
	}
	
	rule @message(inform, string sender, gameOver()) : game(string name, cartago.ArtifactId aid) {
		cartago.println("STOPPING GAME: " + name);
		cartago.stopFocus(aid);
		-game(name, aid);
	}
	
	rule +$cartago.property(string id, round(int round)) : game(string name, cartago.ArtifactId aid) {
		!generateMove(int move);
		-+move(move);
		cartago.operation(aid, makeMove(move));
	}

	rule $cartago.signal(string id, result(int winningMove)) : strategy(string name, cartago.ArtifactId aid) {
		cartago.operation(aid, recordOutcome(winningMove));
	}	
	
	rule +!setupStrategy(string strategy) {
		string strategyName = system.name()+"Strategy";
		cartago.makeArtifact(strategyName, "arena.cartago.StrategyArtifact", cartago.params([2]), cartago.ArtifactId id);
		cartago.operation(id, installStrategy(strategy));
		+strategy(strategyName, id);
	}
	
	rule +!joinGame(string gameName) {
		cartago.lookupArtifact(gameName, cartago.ArtifactId id);
		cartago.focus(id);
		cartago.operation(id, registerPlayer());
		+game(gameName, id);
	}
	
	rule +!generateMove(int move) : strategy(string name, cartago.ArtifactId id) {
		cartago.operation(id, generateMove(move));
	}
}
